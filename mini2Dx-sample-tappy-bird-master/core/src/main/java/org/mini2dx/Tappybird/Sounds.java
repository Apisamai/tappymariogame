/*******************************************************************************
 * Copyright 2019 Viridian Software Limited
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package org.mini2dx.Tappybird;

import org.mini2Dx.core.Mdx;
import org.mini2Dx.core.audio.Music;
import org.mini2Dx.core.audio.Sound;

import java.io.IOException;
import java.util.Random;

public class Sounds{

    private static final String PILLAR_PASS_SOUND_LOCATION = "Sounds/coin1.ogg";
    private static final String DEAD_SOUND_LOCATION = "Sounds/smb_mariodie.wav";
    private static final String BACKGROUND_MUSIC_LOCATION = "Sounds/01 - Super Mario Bros.mp3";
    private static final String JUMPING_SOUND_LOCATION = "Sounds/smb_jump-small.wav";
    private static final String HIGHSCORE_SOUND_LOCATION = "Sounds/smb3_1-up.wav";
    private static final String START_SOUND_1_LOCATION = "Sounds/sm3dl_mario_lets-a_go.wav";
    private static final String START_SOUND_2_LOCATION = "Sounds/smsunshine_mario_ya-hoo.wav";
    private static final String START_SOUND_3_LOCATION = "Sounds/smsunshine_mario_here_we_go.wav";

    static long PILLAR_PASS_SOUND_ID;
    static Sound pillarPassSound;

    static long JUMPING_SOUND_ID;
    static Sound jumpingSound;

    static long HIGHSCORE_SOUND_ID;
    static Sound highscoreSound;

    static long DEAD_SOUND_ID;
    static Sound deadSound;

    static {
        try {
            pillarPassSound = Mdx.audio.newSound(Mdx.files.internal(PILLAR_PASS_SOUND_LOCATION));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    static {
        try {
            jumpingSound = Mdx.audio.newSound(Mdx.files.internal(JUMPING_SOUND_LOCATION));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    static {
        try {
            highscoreSound = Mdx.audio.newSound(Mdx.files.internal(HIGHSCORE_SOUND_LOCATION));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    static {
        try {
            deadSound = Mdx.audio.newSound(Mdx.files.internal(DEAD_SOUND_LOCATION));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    Music backgroundMusic;

    {
        try {
            backgroundMusic = Mdx.audio.newMusic(Mdx.files.internal(BACKGROUND_MUSIC_LOCATION));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    Sound[] startSound;

    {
        try {
            startSound = new Sound[]{Mdx.audio.newSound(Mdx.files.internal(START_SOUND_1_LOCATION)),
                    Mdx.audio.newSound(Mdx.files.internal(START_SOUND_2_LOCATION)),
                    Mdx.audio.newSound(Mdx.files.internal(START_SOUND_3_LOCATION))};
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    void loopBackgroundMusic(){
        backgroundMusic.setVolume(0.25f);
        if(!backgroundMusic.isLooping()){
            backgroundMusic.play();
        }
        backgroundMusic.setLooping(true);
    }

    static void playPillarPassSound(){
        PILLAR_PASS_SOUND_ID = pillarPassSound.play(1.5f);
    }
    static void playJumpSound(){ JUMPING_SOUND_ID = jumpingSound.play(1.5f); }
    static void playHighscoreSound(){ HIGHSCORE_SOUND_ID = highscoreSound.play(1.5f); }
    static void playDeadSound(){
        DEAD_SOUND_ID = deadSound.play(1.5f);
    }
    static void stopDeadSound(){ deadSound.stop();}

    void disposeBackgroundMusic(){
        backgroundMusic.dispose();
    }

    void playStartSound(){
        int index = new Random().nextInt(2);
        startSound[index].play(1f);
    }
}
