package org.mini2dx.Tappybird;

import org.mini2Dx.core.Mdx;
import org.mini2Dx.core.graphics.Texture;

public class ItemTexture {
    private static final String ITEM_TEXTURE_LOCATION = "star_item.png";

    Texture itemTexture = Mdx.graphics.newTexture(Mdx.files.internal(ITEM_TEXTURE_LOCATION));
}
