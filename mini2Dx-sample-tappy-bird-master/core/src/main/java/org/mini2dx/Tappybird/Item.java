package org.mini2dx.Tappybird;

import org.mini2Dx.core.Graphics;
import org.mini2Dx.core.graphics.Texture;

import static org.mini2dx.Tappybird.TappyMarioGame.GAME_HEIGHT;
import static org.mini2dx.Tappybird.TappyMarioGame.GAME_WIDTH;

public class Item {
     Texture itemTexture;
    private int messageX = (int)(GAME_HEIGHT - 300);

    void displayItem(Graphics g)
    {
        g.drawTexture(itemTexture, GAME_WIDTH/2 - itemTexture.getWidth()/2, messageX);
    }
}
